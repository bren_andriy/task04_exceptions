package com.bren.controller;

import com.bren.entity.Employee;
import com.bren.exception.EmployeeNotFoundException;
import com.bren.model.BusinessLogic;
import com.bren.model.EmployeeModel;


public class EmployeeControllerImpl implements EmployeeController{
    private EmployeeModel employeeModel;

    public EmployeeControllerImpl() {
        employeeModel = new BusinessLogic();
    }

    @Override
    public void addEmployee(Employee employee) throws Exception {
        employeeModel.addEmployee(employee);
    }

    @Override
    public void deleteEmployee(int index) throws EmployeeNotFoundException {
        employeeModel.deleteEmployee(index);
    }

    @Override
    public void deleteEmployeeById(int id) {
        employeeModel.deleteEmployeeById(id);
    }

    @Override
    public void printEmployeeList() {
        employeeModel.printEmployeeList();
    }
}
