package com.bren.controller;

import com.bren.entity.Employee;
import com.bren.exception.EmployeeNotFoundException;


public interface EmployeeController {
    void addEmployee(Employee employee) throws Exception;
    void deleteEmployee(int index) throws EmployeeNotFoundException;
    void deleteEmployeeById(int id);
    void printEmployeeList();
}
