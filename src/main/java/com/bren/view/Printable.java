package com.bren.view;

@FunctionalInterface
public interface Printable {
    void print() throws Exception;
}
