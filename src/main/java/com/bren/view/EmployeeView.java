package com.bren.view;

import com.bren.controller.EmployeeController;
import com.bren.controller.EmployeeControllerImpl;
import com.bren.entity.Employee;
import com.bren.exception.EmployeeNotFoundException;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class EmployeeView implements Serializable {
    private EmployeeController employeeController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public EmployeeView() {
        employeeController = new EmployeeControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - printAllEmployee");
        menu.put("2", "  2 - addEmployeeToCompany");
        menu.put("3", "  3 - deleteEmployee");
        menu.put("4", "  4 - deleteEmployeeById");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        employeeController.printEmployeeList();
    }

    private void pressButton2() throws Exception {
        System.out.print("Enter name: ");
        String name = input.nextLine();
        System.out.print("Enter employee id: ");
        int id = Integer.parseInt(input.nextLine());
        System.out.print("Enter salary: ");
        int salary = Integer.parseInt(input.nextLine());
        try (Employee employee = new Employee(name,id,salary)) {
            employeeController.addEmployee(employee);
        }
    }

    private void pressButton3() {
        System.out.println("Enter index: ");
        int index = Integer.parseInt(input.nextLine());
        try {
            employeeController.deleteEmployee(index - 1);
        } catch (EmployeeNotFoundException e) {
            e.printStackTrace();
            System.out.println("You should add one employee to your company at least");
        }
    }

    private void pressButton4() {
        System.out.println("Enter id: ");
        int id = Integer.parseInt(input.nextLine());
        employeeController.deleteEmployeeById(id);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
