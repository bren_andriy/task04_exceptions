package com.bren.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Getter
@Setter
public class Employee implements AutoCloseable {
    private String name;
    private int salary;
    private LocalDateTime dateOfRecruitment;
    private LocalDateTime dateOfEndRecruitment;
    private int id;
    private static int employeeCounter = 0;
    private PrintWriter file;

    public Employee(String name, int id, int salary) {
        this.name = name;
        this.id = id;
        this.salary = salary;
        this.dateOfRecruitment = LocalDateTime.now();
        employeeCounter++;
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        System.out.println();
        System.out.println("Name: " + this.name);
        System.out.println("Id : " + this.id);
        System.out.println("Salary : " + this.salary);
        System.out.println("Date of Recruitment: " + this.dateOfRecruitment.format(formatter));
        System.out.println("Number of employees in company: " + employeeCounter);
        try {
            file = new PrintWriter(this.name + ".txt");
            this.dateOfRecruitment = LocalDateTime.now();
            file.println();
            file.println("Name: " + name);
            file.println("Id : " + id);
            file.println("Salary : " + salary);
            file.println("Date of Recruitment: " + dateOfRecruitment.format(formatter));
            file.println("Number of employees in company: " + employeeCounter);
        } catch (FileNotFoundException e) {
            System.out.println("File not created");
        }
    }

    @Override
    public void close() throws Exception {
        if (employeeCounter == 5) {
            throw new IOException("Exception: 'method close()'");
        }
        this.dateOfEndRecruitment = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        System.out.println("=======");
        System.out.println("Date of end recruitment: " + dateOfEndRecruitment.format(formatter));
        if (file != null) {
            file.println();
            file.println("Date of end recruitment: " + dateOfEndRecruitment.format(formatter));
            file.close();
        }
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", dateOfRecruitment=" + dateOfRecruitment +
                ", id=" + id +
                '}';
    }
}
