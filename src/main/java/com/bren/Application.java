package com.bren;

import com.bren.view.EmployeeView;

public class Application {
    public static void main(String[] args) {
        new EmployeeView().show();
    }
}
