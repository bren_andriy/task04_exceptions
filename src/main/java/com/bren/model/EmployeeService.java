package com.bren.model;

import com.bren.entity.Employee;
import com.bren.exception.EmployeeNotFoundException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

class EmployeeService implements Serializable {
    private List<Employee> employees;

    EmployeeService() {
        employees = new ArrayList<>();
    }

    void addEmployee(Employee employee) {
        employees.add(employee);
    }

    void deleteEmployee(int index) throws EmployeeNotFoundException {
        if (employees.isEmpty()) {
            throw new EmployeeNotFoundException("Can`t delete employee with index " + index);
        }
        employees.remove(index);
    }

    void deleteEmployeeById(int id) {
        employees.removeIf(employee -> employee.getId() == id);
    }

    private List<Employee> getEmployees() {
        return new ArrayList<>(employees);
    }

    void printEmployeeList() {
        ListIterator<Employee> iterator = getEmployees().listIterator();
        while (iterator.hasNext()) {
            int i = iterator.nextIndex();
            System.out.println(i + 1 + ". " + iterator.next());
        }
    }
}
