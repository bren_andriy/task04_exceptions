package com.bren.model;

import com.bren.entity.Employee;
import com.bren.exception.EmployeeNotFoundException;

import java.io.Serializable;


public class BusinessLogic implements EmployeeModel, Serializable {
    private EmployeeService employeeService;

    public BusinessLogic() {
        employeeService = new EmployeeService();
    }

    @Override
    public void addEmployee(Employee employee){
        employeeService.addEmployee(employee);
    }

    @Override
    public void deleteEmployee(int index) throws EmployeeNotFoundException {
        employeeService.deleteEmployee(index);
    }

    @Override
    public void deleteEmployeeById(int id) {
        employeeService.deleteEmployeeById(id);
    }

    @Override
    public void printEmployeeList() {
        employeeService.printEmployeeList();
    }
}
