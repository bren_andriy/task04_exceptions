package com.bren.model;

import com.bren.entity.Employee;
import com.bren.exception.EmployeeNotFoundException;


public interface EmployeeModel {
    void addEmployee(Employee employee) throws Exception;
    void deleteEmployee(int index) throws EmployeeNotFoundException;
    void deleteEmployeeById(int id);
    void printEmployeeList();

}
